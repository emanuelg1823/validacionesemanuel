<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

/**
 * Class Contacto
 *
 * @property $id
 * @property $Nombre
 * @property $Apellido
 * @property $Cedula
 * @property $Cargo
 * @property $Direccion
 * @property $Email
 * @property $Telefono
 * @property $created_at
 * @property $updated_at
 *
 * @package App
 * @mixin \Illuminate\Database\Eloquent\Builder
 */
class Contacto extends Model
{
    
    static $rules = [
		'Nombre' => 'required',
		'Apellido' => 'required',
		'Cedula' => 'required',
		'Cargo' => 'required',
		'Direccion' => 'required',
		'Email' => 'required',
		'Telefono' => 'required',
    ];

    protected $perPage = 20;

    /**
     * Attributes that should be mass-assignable.
     *
     * @var array
     */
    protected $fillable = ['Nombre','Apellido','Cedula','Cargo','Direccion','Email','Telefono'];



}
